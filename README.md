# Codeline's Frontend Test

> A technical test that be part from Codeline's recruitment process.

## Clone this repository and enter the folder

```
git clone https://lucascavalcante@bitbucket.org/lucascavalcante/codeline-frontend.git
cd codeline-frontend
```

## Run PHP server

```
php -S 127.0.0.1:8000 -t api/
```

## Build Setup

``` bash

# enter the client folder
cd client/

# install dependencies
yarn install

# serve with hot reload at localhost:8080
npm run dev

# access on browser
http://localhost:8080/

```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
