import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000';

export default {
    searchKeyword(keyword) {
        let url = API_URL + '/weather.php?command=search&keyword=' + keyword;
        return axios.get(url).then(response => response.data);
    },
    getLocation(woeid) {
        let url = API_URL + '/weather.php?command=location&woeid=' + woeid;
        return axios.get(url).then(response => response.data);
    }
}